# Projeto 2 (Servidor distribuído) - Fundamentos de sistemas embarcados

## Servidor central

O link para o git do servidor central é o seguinte [servidor central](https://gitlab.com/gustavolima00/projeto-2-servidor-central-fundamentos-de-sistemas-embarcados)

## Servidor distribuído

Sistema que consiste no controle dos componentes periféricos o controle é feito via conexão TCP com o seguinte formato

### Formato da requisição

#### Cabeçalho 
(1 bytes) Tipo de requisição
- 0x0 : Solicitação dos estados de cada componente
- 0x1 : Solicitação para ligar um componente
- 0x2 : Solicitação para desligar um componente  
- 0x3 : Solicitação para mudar estado de um componente  
- 0x4 : Definir temperatura de referencia (controle automatico da temperatura)

#### Corpo 
##### 0x0

(Vazio)

##### 0x1-0x3

(1 byte) Número do componente
- 0x0 : Lâmpada 01 (Cozinha)
- 0x1 : Lâmpada 02 (Sala)
- 0x2 : Lâmpada 03 (Quarto 01)
- 0x3 : Lâmpada 04 (Quarto 02)
- 0x4 : Ar-Condicionado 01 (Quarto 01)
- 0x5 : Ar-Condicionado 02 (Quarto 02)

##### 0x4

(4 bytes) Temperatura de referencia (float)
'
### Formato da resposta

#### Cabeçalho 

(1 byte) Resultado da requisição
- 0x0 : Sucesso na requisição
- 0x1 : Erro no formato da mensagem
- 0x2 : Erro interno do servidor

#### Corpo (Caso da solicitação)

(4 bytes) Temperatura

(4 bytes) Temperatura de referencia

(4 bytes) Humidade

(1 byte) Estado da Lâmpada 01 (Cozinha)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Lâmpada 02 (Sala)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Lâmpada 03 (Quarto 01)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Lâmpada 04 (Quarto 02)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Ar-Condicionado 01 (Quarto 01)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Ar-Condicionado 02 (Quarto 02)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Sensor de Presença 01 (Sala)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Sensor de Presença 02 (Cozinha)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Sensor Abertura 01 (Porta Cozinha)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Sensor Abertura 02 (Janela Cozinha)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Sensor Abertura 03 (Porta Sala)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Sensor Abertura 04 (Janela Sala)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Sensor Abertura 05 (Janela Quarto 01)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Sensor Abertura 06 (Janela Quarto 02)
- 0x0 Desligada
- 0x1 Ligada

(1 byte) Controle automático da temperatura
- 0x0 Desligada
- 0x1 Ligada