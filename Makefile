BINFOLDER := bin/
INCFOLDER := inc/
SRCFOLDER := src/
OBJFOLDER := obj/

CC := g++
CFLAGS := -Wall -Wextra -std=c++17 -lwiringPi 
PFLAGS := -lwiringPi -lbcm2835 -lncurses -pthread

SRCFILES := $(wildcard src/*.cpp src/*.c)

all: $(SRCFILES:src/%.cpp=obj/%.o)
	$(CC) $(CFLAGS) obj/*.o -o bin/main $(PFLAGS) 

obj/%.o: src/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ -I./inc $(PFLAGS) 

.PHONY: clean
clean:
	rm -rf obj/*
	rm -rf bin/*

run:
		bin/main

