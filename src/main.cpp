#include <iostream>
#include "gpio.hpp"
#include "i2c.hpp"
#include "network.hpp"
#include <thread>
#include <signal.h>
#include <unordered_map>
#include <math.h>
using namespace std;

const double HISTERESYS = 2.0;

/* Pin variables */
GPIOPin
	*lamp1,
	*lamp2, *lamp3, *lamp4,
	*air1, *air2,
	*motion1, *motion2,
	*opening1, *opening2, *opening3, *opening4, *opening5, *opening6;

BME280 *bme_sensor;
TCPServer *server;

/* Threads Variables */
thread server_thread, sensor_thread, temperature_thread;
bool server_start, server_finish;
bool sensor_start, sensor_finish;
bool temperature_start, temperature_finish;

unordered_map<int, GPIOPin *> pin;
bool auto_temperature;
bool running;
short int count = 0;
double refer_temperature;

void connectionHandler(int);
void sig_handler(int signum);
void stop_execution(int signum);
void sensor_alert(int device_id);
void default_handler(int socket);

int main()
{
	signal(SIGALRM, sig_handler);
	signal(SIGTERM, stop_execution);
	signal(SIGKILL, stop_execution);
	signal(SIGINT, stop_execution);
	running = true;
	auto_temperature = true;
	refer_temperature = 27.0;
	initGPIO();

	/* Alocando pins */
	lamp1 = new GPIOPin(LAMP1, OUTPUT);
	pin[0x0] = lamp1;
	lamp2 = new GPIOPin(LAMP2, OUTPUT);
	pin[0x1] = lamp2;
	lamp3 = new GPIOPin(LAMP3, OUTPUT);
	pin[0x2] = lamp3;
	lamp4 = new GPIOPin(LAMP4, OUTPUT);
	pin[0x3] = lamp4;

	air1 = new GPIOPin(AIR1, OUTPUT);
	pin[0x4] = air1;
	air2 = new GPIOPin(AIR2, OUTPUT);
	pin[0x5] = air2;

	motion1 = new GPIOPin(MOTION1, INPUT, [](){sensor_alert(0x6);});
	pin[0x6] = motion1;
	motion2 = new GPIOPin(MOTION2, INPUT, [](){sensor_alert(0x7);});
	pin[0x7] = motion2;

	opening1 = new GPIOPin(OPENING1, INPUT, [](){sensor_alert(0x8);});
	pin[0x8] = opening1;
	opening2 = new GPIOPin(OPENING2, INPUT, [](){sensor_alert(0x9);});
	pin[0x9] = opening2;
	opening3 = new GPIOPin(OPENING3, INPUT, [](){sensor_alert(0xA);});
	pin[0xA] = opening3;
	opening4 = new GPIOPin(OPENING4, INPUT, [](){sensor_alert(0xB);});
	pin[0xB] = opening4;
	opening5 = new GPIOPin(OPENING5, INPUT, [](){sensor_alert(0xC);});
	pin[0xC] = opening5;
	opening6 = new GPIOPin(OPENING6, INPUT, [](){sensor_alert(0xD);});
	pin[0xD] = opening6;

	bme_sensor = new BME280("/dev/i2c-1");
	server = new TCPServer("10125", connectionHandler);

	ualarm(100000, 100000);
	while (running)
		pause();
}
void stop_execution(int signum)
{
	for(int i=0; i<6; ++i)
		pin[i]->setPinState(LOW);
	for(int i=0; i<14; ++i)
		delete pin[i];
	delete bme_sensor;
	delete server;
	cout << "Finalizando execução\n";
	running = false;
}
void sig_handler(int signum)
{
	count = (count + 1) % 20;
	if (not server_start)
	{
		server_start = true;
		server_finish = false;
		server_thread = thread([]() {
			server->acceptConnection();
			cout << "Get connection" << endl;
			server_finish = true;
		});
	}
	// Every 1 second
	if (count % 10 == 0 and not sensor_start)
	{
		sensor_start = true;
		sensor_finish = false;
		sensor_thread = thread([]() {
			bme_sensor->updateData();
			for (int i = 0; i < 14; ++i)
				pin[i]->update();
			sensor_finish = true;
		});
	}
	// Every 1 second
	if(count%10 == 0 and auto_temperature and not temperature_start){
		temperature_start = true;
		temperature_finish = false;
		temperature_thread = thread([](){
			double temperature = bme_sensor->getTemperature();
			if(fabs(temperature-refer_temperature)<HISTERESYS/2.0 or temperature>refer_temperature)
			{
				air1->setPinState(HIGH);
				air2->setPinState(HIGH);
			}
			else
			{
				air1->setPinState(LOW);
				air2->setPinState(LOW);
			}
			temperature_finish = true;
		});
	}

	/* Threads join */
	if (temperature_start and temperature_finish)
	{
		temperature_thread.join();
		temperature_start = false;
	}
	if (server_start and server_finish)
	{
		server_thread.join();
		server_start = false;
	}
	if (sensor_start and sensor_finish)
	{
		sensor_thread.join();
		sensor_start = false;
	}
}
void connectionHandler(int socket)
{
	uint8_t header;
	int rcv_bytes = recv(socket, &header, sizeof(header), 0);
	if (rcv_bytes < 0)
	{
		cerr << "Erro no recv()" << endl;
		return;
	}
	uint8_t RESPONSE_OK = 0x0, BAD_REQUEST = 0x1;
	uint8_t component_id;
	switch (header)
	{
	case 0x0:
	{
		float r_temp, r_refer, r_hum;
		char pin_buffer[14];
		uint8_t auto_temp = auto_temperature ? 0x1:0x0;
		r_temp = bme_sensor->getTemperature();
		r_refer = refer_temperature;
		r_hum = bme_sensor->getHumidity();

		for (int i = 0; i < 14; ++i)
			pin_buffer[i] = (pin[i]->getPinState() ? 0x1 : 0x0);

		send(socket, &RESPONSE_OK, sizeof(uint8_t), 0);
		send(socket, &r_temp, sizeof(float), 0);
		send(socket, &r_refer, sizeof(float), 0);
		send(socket, &r_hum, sizeof(float), 0);
		send(socket, pin_buffer, 14, 0);
		send(socket, &auto_temp, sizeof(uint8_t), 0);
	}
	break;
	case 0x1:
	{
		rcv_bytes = recv(socket, &component_id, sizeof(component_id), 0);
		if (rcv_bytes < 0)
		{
			cerr << "Erro no recv()" << endl;
			return;
		}
		if (component_id < 0x6)
		{
			pin[component_id]->setPinState(HIGH);
			send(socket, &RESPONSE_OK, sizeof(RESPONSE_OK), 0);
			if (component_id == 0x4 or component_id == 0x5) // AR condicionado
			{
				auto_temperature = false;
			}
		}
		else
		{
			send(socket, &BAD_REQUEST, sizeof(BAD_REQUEST), 0);
		}
	}
	break;
	case 0x2:
	{
		rcv_bytes = recv(socket, &component_id, sizeof(component_id), 0);
		if (rcv_bytes < 0)
		{
			cerr << "Erro no recv()" << endl;
			return;
		}
		if(component_id < 0x6)
		{
			pin[component_id]->setPinState(LOW);
			send(socket, &RESPONSE_OK, sizeof(RESPONSE_OK), 0);
			if (component_id == 0x4 or component_id == 0x5) // AR condicionado
			{
				auto_temperature = false;
			}
		}
		else
		{
			send(socket, &BAD_REQUEST, sizeof(BAD_REQUEST), 0);
		}
	}
	break;
	case 0x3:
	{
		rcv_bytes = recv(socket, &component_id, sizeof(component_id), 0);
		if (rcv_bytes < 0)
		{
			cerr << "Erro no recv()" << endl;
			return;
		}
		if (component_id < 0x6)
		{
			if (pin[component_id]->getPinState())
				pin[component_id]->setPinState(LOW);
			else
				pin[component_id]->setPinState(HIGH);
			send(socket, &RESPONSE_OK, sizeof(RESPONSE_OK), 0);
			if (component_id == 0x4 or component_id == 0x5) // AR condicionado
			{
				auto_temperature = false;
			}
		}
		else
		{
			send(socket, &BAD_REQUEST, sizeof(BAD_REQUEST), 0);
		}
	}
	break;
	case 0x4:
	{
		float r_temp;
		rcv_bytes = recv(socket, &r_temp, sizeof(float), 0);
		if (rcv_bytes < 0)
		{
			cerr << "Erro no recv()" << endl;
			return;
		}
		refer_temperature = r_temp;
		auto_temperature = true;
		send(socket, &RESPONSE_OK, sizeof(RESPONSE_OK), 0);
	}
	break;
	default:
	{
		send(socket, &BAD_REQUEST, sizeof(BAD_REQUEST), 0);
	}
	}
}
void sensor_alert(int device_id){
	char buffer[2];
	buffer[0] = 0x0;
	buffer[1] = device_id;
	try{
		send_message((char *)CENTRAL_SERVER_IP.c_str(), (char *)CENTRAL_SERVER_PORT.c_str(), buffer, 2, default_handler);
	}
	catch(server_error& e){
		cerr << e.get_message() << endl;
		return;
	}
}
void default_handler(int socket){
	uint8_t header;
    int rcv_bytes = recv(socket, &header, sizeof(uint8_t), 0);
    if (rcv_bytes<0 or header != 0x0)
    {
        cerr << "Erro na comunicação com o servidor" << endl;
        return;
    }
}

