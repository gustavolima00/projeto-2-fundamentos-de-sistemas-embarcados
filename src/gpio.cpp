#include <bcm2835.h>
#include "gpio.hpp"
#include <iostream>
#include <thread>
using namespace std;

void initGPIO(){
    if (!bcm2835_init()){
        cerr << "Erro ao iniciar bcm2835\n";
        exit(1);
    };
}

GPIOPin::GPIOPin(int pin_number, int pin_mode, void (*onTurnOn)(void)){
    this->onTurnOn = onTurnOn;
    if(pin_mode!=INPUT and pin_mode!=OUTPUT){
        cerr << "Invalid pin_mode value\n";
        exit(1);
    }
    this->pin_number = pin_number;
    this->pin_mode = pin_mode;
}

void GPIOPin::setPinState(int state){
    if(this->pin_mode == INPUT){
        cerr << "Unable to set a INPUT pin\n";
        exit(1);
    }
    bcm2835_gpio_write(this->pin_number, state);
}

bool GPIOPin::getPinState(){
    return this->state;
}
void GPIOPin::update(){
    bool new_state = bcm2835_gpio_lev(this->pin_number) == HIGH;
    if(new_state and not this->state) thread(this->onTurnOn).detach();
    this->state = new_state;
}