#include "i2c.hpp"
#include <iostream>
#include <string>
#include <unistd.h>
using namespace std;

BME280::BME280(){
    
}
BME280::BME280(const char *i2c_bus){
    if ((this->id.fd = open(i2c_bus, O_RDWR)) < 0)
    {
        cerr << "Failed to open the i2c bus\n";
        exit(1);
    }
	this->id.dev_addr = BME280_I2C_ADDR_PRIM;
    if (ioctl(this->id.fd, I2C_SLAVE, this->id.dev_addr) < 0)
    {
		cerr << "Failed to acquire bus access and/or talk to slave.\n";
        exit(1);
    }
	this->dev.intf = BME280_I2C_INTF;
    this->dev.read = user_i2c_read;
    this->dev.write = user_i2c_write;
    this->dev.delay_us = user_delay_us;
	this->dev.intf_ptr = &id;
    
    int8_t rslt = BME280_OK;
	rslt = bme280_init(&this->dev);
    if (rslt != BME280_OK)
    {
		cerr << "Failed to initialize the device (code " << rslt << ").\n";
        exit(1);
    }
}
void BME280::updateData(){
	int8_t rslt = BME280_OK;
    rslt = stream_sensor_data_forced_mode(&this->dev, this->temp, this->press, this->hum);
    if (rslt != BME280_OK)
    {
		cerr << "Failed to stream sensor data (code " << rslt << ").\n";
        exit(1);
    }
}

double BME280::getTemperature(){
	return this->temp;
}
double BME280::getPressure(){
	return this->press;
}
double BME280::getHumidity(){
	return this->hum;
}
