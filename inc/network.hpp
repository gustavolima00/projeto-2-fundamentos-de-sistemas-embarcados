#ifndef NETWORK_H
#define NETWORK_H
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
using namespace std;

extern const string DISTRIBUTED_SERVER_IP;
extern const string DISTRIBUTED_SERVER_PORT;
extern const string CENTRAL_SERVER_IP;
extern const string CENTRAL_SERVER_PORT;

class TCPServer{
private:
	int servidorSocket;
	struct sockaddr_in servidorAddr;
	unsigned short servidorPorta;
	void (*connectionHandler)(int);
public:
	TCPServer();
	TCPServer(const char* port, void (*connectionHandler)(int));
	~TCPServer();
	void acceptConnection();
};

struct server_error : public std::exception
{
	const char *message;
	server_error(const char *s);
	const char* get_message();
};

int send_message(char* server_ip, char* server_port, char* message, size_t tamanhoMensagem);
void send_message(char* server_ip, char* server_port, char* message, size_t tamanhoMensagem, void (*response_handler)(int));
#endif