#ifndef I2C_H
#define I2C_H
#include <iostream>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include "bme280.hpp"

class BME280{
	private:
	struct bme280_dev dev;
    struct identifier id;
	double temp, press, hum;
	public:
		BME280();
		BME280(const char *i2c_bus);
		void updateData();
		double getTemperature();
		double getPressure();
		double getHumidity();
};

#endif
