#ifndef BME280_H_
#define BME280_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bme280_defs.hpp"

struct identifier{
    uint8_t dev_addr;
    int8_t fd;
};

#include "bme280.hpp"

#define OVERSAMPLING_SETTINGS    UINT8_C(0x07)

#define FILTER_STANDBY_SETTINGS  UINT8_C(0x18)

inline int8_t put_device_to_sleep(struct bme280_dev *dev);

inline int8_t write_power_mode(uint8_t sensor_mode, struct bme280_dev *dev);

inline int8_t null_ptr_check(const struct bme280_dev *dev);

inline void interleave_reg_addr(const uint8_t *reg_addr, uint8_t *temp_buff, const uint8_t *reg_data, uint8_t len);

inline int8_t get_calib_data(struct bme280_dev *dev);

inline void parse_temp_press_calib_data(const uint8_t *reg_data, struct bme280_dev *dev);

inline void parse_humidity_calib_data(const uint8_t *reg_data, struct bme280_dev *dev);

#ifdef BME280_FLOAT_ENABLE

inline double compensate_pressure(const struct bme280_uncomp_data *uncomp_data,
                                  const struct bme280_calib_data *calib_data);

inline double compensate_humidity(const struct bme280_uncomp_data *uncomp_data,
                                  const struct bme280_calib_data *calib_data);

inline double compensate_temperature(const struct bme280_uncomp_data *uncomp_data,
                                     struct bme280_calib_data *calib_data);

#else

inline int32_t compensate_temperature(const struct bme280_uncomp_data *uncomp_data,
                                      struct bme280_calib_data *calib_data);

inline uint32_t compensate_pressure(const struct bme280_uncomp_data *uncomp_data,
                                    const struct bme280_calib_data *calib_data);

inline uint32_t compensate_humidity(const struct bme280_uncomp_data *uncomp_data,
                                    const struct bme280_calib_data *calib_data);

#endif

inline uint8_t are_settings_changed(uint8_t sub_settings, uint8_t desired_settings);

inline int8_t set_osr_humidity_settings(const struct bme280_settings *settings, struct bme280_dev *dev);

inline int8_t set_osr_settings(uint8_t desired_settings, const struct bme280_settings *settings,
                               struct bme280_dev *dev);

inline int8_t set_osr_press_temp_settings(uint8_t desired_settings,
                                          const struct bme280_settings *settings,
                                          struct bme280_dev *dev);

inline void fill_osr_press_settings(uint8_t *reg_data, const struct bme280_settings *settings);

inline void fill_osr_temp_settings(uint8_t *reg_data, const struct bme280_settings *settings);

inline int8_t set_filter_standby_settings(uint8_t desired_settings,
                                          const struct bme280_settings *settings,
                                          struct bme280_dev *dev);

inline void fill_filter_settings(uint8_t *reg_data, const struct bme280_settings *settings);

inline void fill_standby_settings(uint8_t *reg_data, const struct bme280_settings *settings);

inline void parse_device_settings(const uint8_t *reg_data, struct bme280_settings *settings);

inline int8_t reload_device_settings(const struct bme280_settings *settings, struct bme280_dev *dev);

int8_t bme280_init(struct bme280_dev *dev);

int8_t bme280_set_regs(uint8_t *reg_addr, const uint8_t *reg_data, uint8_t len, struct bme280_dev *dev);

int8_t bme280_get_regs(uint8_t reg_addr, uint8_t *reg_data, uint16_t len, struct bme280_dev *dev);

int8_t bme280_set_sensor_settings(uint8_t desired_settings, struct bme280_dev *dev);

int8_t bme280_get_sensor_settings(struct bme280_dev *dev);

int8_t bme280_set_sensor_mode(uint8_t sensor_mode, struct bme280_dev *dev);

int8_t bme280_get_sensor_mode(uint8_t *sensor_mode, struct bme280_dev *dev);

int8_t bme280_soft_reset(struct bme280_dev *dev);

int8_t bme280_get_sensor_data(uint8_t sensor_comp, struct bme280_data *comp_data, struct bme280_dev *dev);

void bme280_parse_sensor_data(const uint8_t *reg_data, struct bme280_uncomp_data *uncomp_data);

int8_t bme280_compensate_data(uint8_t sensor_comp,
                              const struct bme280_uncomp_data *uncomp_data,
                              struct bme280_data *comp_data,
                              struct bme280_calib_data *calib_data);

uint32_t bme280_cal_meas_delay(const struct bme280_settings *settings);

void user_delay_us(uint32_t period, void *intf_ptr);


int8_t user_i2c_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void *intf_ptr);

int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr);

int8_t stream_sensor_data_forced_mode(struct bme280_dev *dev, double &temp, double &press, double &hum);

#ifdef __cplusplus
}
#endif /* End of CPP guard */
#endif /* BME280_H_ */
/** @}*/
